<?php

// Vide une table SQL
function db_empty_table($db, $table_name) {
	$req = 'DELETE FROM ' . $table_name;
	return $db->query($req);
}

// Fonction qui change le format d'une date
function date_change_format($date, $format = 'd/m/Y', $new_format = 'Y-m-d') {
	$date = DateTime::createFromFormat($format, $date);
	return $date->format($new_format);
}

// Fonction qui met au format FR un numéro de téléphone
function date_clean_phone($tel) {
	return '0' . substr($tel, -9);
}