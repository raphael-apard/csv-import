<?php
require 'config/settings.php';
require 'inc/db.php';
require 'inc/helpers.php';

const CSV_FILE_PATH = 'data/people.csv';
const CSV_FILE_NEW_PATH = 'data/people-clean.csv';

// En début de script je stock la date
$time_start = microtime(true); 

// On vide la table
db_empty_table($db, 'people');

// Ouvrir le fichier CSV
$stream = fopen(CSV_FILE_PATH, 'r');
$new_csv = fopen(CSV_FILE_NEW_PATH, 'w');
if (!$stream) {
	die("Le fichier " . CSV_FILE_PATH . " est introuvable");
}

// Je boucle sur le stream du fichier
$count = 0;
while($row = fgetcsv($stream)) {
	if ($row[0] === 'id') {
		fputcsv($new_csv, $row);
		continue;
	}

	// Convertion du format de la date
	$row[3] = strtolower($row[3]);
	$row[3] = filter_var($row[3], FILTER_VALIDATE_EMAIL) ? $row[3] : '';
	$row[4] = str_replace('politicien', 'menteur', $row[4]);
	$row[5] = date_change_format($row[5]);
	$row[6] = Locale::getDisplayRegion("-" . $row[6] , 'fr');
	$row[7] = date_clean_phone($row[7]);

	// Insertion dans le nouveau fichier	
	fputcsv($new_csv, $row);

	// Insertion en base 
	$req = "INSERT INTO people VALUES(?,?,?,?,?,?,?,?)";
	$res = $db->prepare($req);
	
	if ($res->execute($row)) {
		$count++;
	}

}

// Message de confirmation
echo "$count personnes correctement importés en base de données\n";

// Fermeture des fichiers
fclose($stream);
fclose($new_csv);

// A la fin du script je récupére la date
$time_end = microtime(true);

// Je calcule la différence (en seconde)
$execution_time = ($time_end - $time_start);

// J'affiche le résultat
echo "Temps d'execution: ".$execution_time."sec";